#!/bin/bash

#SBATCH --job-name=cellranger_gex     #Name of your job
#SBATCH --cpus-per-task=20    #Number of cores to reserve
#SBATCH --mem-per-cpu=6G     #Amount of RAM/core to reserve
#SBATCH --time=24:00:00      #Maximum allocated time
#SBATCH --qos=1day         #Selected queue to allocate your job
#SBATCH --output=cellranger_gex.log   #Path and name to the file for the STDOUT
#SBATCH --mail-type=END,FAIL,TIME_LIMIT
#SBATCH --mail-user=julian.spagnuolo@unibas.ch        #You will be notified via email when your task ends or fails


module load CellRanger/5.0.0

FastqIn=$1
SampleID=$2
sampleName=$3
expected=$4

cellranger count --localcores 20 --localmem 120 --transcriptome ~/genomes/gex_hsap_txg/GRCh38/ --fastqs $FastqIn --id $SampleID --sample $sampleName --expect-cells $expected --nosecondary


#!/bin/bash

#SBATCH --job-name=cellranger_vdj     #Name of your job
#SBATCH --cpus-per-task=20    #Number of cores to reserve
#SBATCH --mem-per-cpu=6G     #Amount of RAM/core to reserve
#SBATCH --time=06:00:00      #Maximum allocated time
#SBATCH --qos=6hours         #Selected queue to allocate your job
#SBATCH --output=cellranger.log   #Path and name to the file for the STDOUT
#SBATCH --mail-type=END,FAIL,TIME_LIMIT
#SBATCH --mail-user=julian.spagnuolo@unibas.ch        #You will be notified via email when your task ends or fails


module load CellRanger/5.0.0

FastqIn=$1
VDJout=$2

cellranger vdj --reference ~/genomes/vdj_hsap_txg/ --fastqs $FastqIn --chain TR --localcores 20 --localmem 120 --id $VDJout


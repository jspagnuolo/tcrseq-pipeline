#!/bin/bash


#SBATCH --job-name=PE_trimmo                   #This is the name of your job
#SBATCH --cpus-per-task=1                  #This is the number of cores reserved
#SBATCH --mem-per-cpu=8G              #This is the memory reserved per core.
#Total memory reserved: 8GB

#SBATCH --time=1-00:00:00        #This is the time that your task will run
#SBATCH --qos=1day           #You will run in this queue

# Paths to STDOUT or STDERR files should be absolute or relative to current working directory
#SBATCH --output=pe_trimmo.o     #These are the STDOUT and STDERR files
#SBATCH --error=pe_trimmo.e

#This job runs from the current working directory


#Remember:
#The variable $TMPDIR points to the local hard disks in the computing nodes.
#The variable $HOME points to your home directory.
#The variable $SLURM_JOBID stores the ID number of your job.


fwd=$1
rev=$2

pathin=$3
pathout=$4

module load Trimmomatic/0.39-Java-1.8

java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.39.jar PE $pathin/$fwd $pathin/$rev $pathout/${fwd[@]/.fastq.gz/_trim.fastq.gz} $pathout/unpaired/${fwd[@]/.fastq.gz/_trim_up.fastq.gz} $pathout/${rev[@]/.fastq.gz/_trim.fastq.gz} $pathout/unpaired/${rev[@]/.fastq.gz/_trim_up.fastq.gz} ILLUMINACLIP:$HOME/genomes/all_illumina_clip.fa:2:30:10:2:keepBothReads LEADING:3 TRAILING:3 SLIDINGWINDOW:8:18 MINLEN:50


#!/bin/bash

#SBATCH --job-name=tcr_seqtk_pe_trim                   #This is the name of your job
#SBATCH --cpus-per-task=1                  #This is the number of cores reserved
#SBATCH --mem-per-cpu=20G              #This is the memory reserved per core.

#SBATCH --time=6:00:00        #This is the time that your task will run
#SBATCH --qos=6hours           #You will run in this queue

# Paths to STDOUT or STDERR files should be absolute or relative to current working directory
#SBATCH --output=tcr_seqtk_pe_trim     #This is the joined STDOUT and STDERR file
#SBATCH --mail-type=END,FAIL,TIME_LIMIT
#SBATCH --mail-user=julian.spagnuolo@unibas.ch        #You will be notified via email when your task ends or fails




#Remember:
#The variable $TMPDIR points to the local hard disks in the computing nodes.
#The variable $HOME points to your home directory.
#The variable $JOB_ID stores the ID number of your task.


#load your required modules below
#################################

module load Seqtk/1.2-goolf-1.7.20-r94
#module load Trimmomatic/0.39-Java-1.8

filein=$1


while read i; do
    seqtk mergepe ${i/%/_1.fastq} ${i/%/_2.fastq} | seqtk trimfq -q 0.02 -l 130 - | seqtk dropse > R12.trim
    seqtk seq -1 R12.trim | gzip > ${i/%/_trim_1.fastq.gz}
    seqtk seq -2 R12.trim | gzip > ${i/%/_trim_2.fastq.gz}
    #java -jar trimmomatic-0.39.jar PE -phred33 ${i/%/_1.fastq} ${i/%/_2.fastq} ${i/%/_trim_1.fastq} ${i/%/_trim_up_1.fq} ${i/%/_trim_2.fastq} ${i/%/_trim_up_2.fq} TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:130
done <$filein

rm R12.trim
#!/bin/bash

#SBATCH --job-name=tcr_seqtk_trim                   #This is the name of your job
#SBATCH --cpus-per-task=1                  #This is the number of cores reserved
#SBATCH --mem-per-cpu=12G              #This is the memory reserved per core.

#SBATCH --time=6:00:00        #This is the time that your task will run
#SBATCH --qos=6hours           #You will run in this queue

# Paths to STDOUT or STDERR files should be absolute or relative to current working directory
#SBATCH --output=tcr_seqtk_trim     #This is the joined STDOUT and STDERR file
#SBATCH --mail-type=END,FAIL,TIME_LIMIT
#SBATCH --mail-user=julian.spagnuolo@unibas.ch        #You will be notified via email when your task ends or fails




#Remember:
#The variable $TMPDIR points to the local hard disks in the computing nodes.
#The variable $HOME points to your home directory.
#The variable $JOB_ID stores the ID number of your task.


#load your required modules below
#################################

pathin=$1

module load Seqtk/1.2-goolf-1.7.20-r94

cd $pathin
mkdir trim

for i in ./*.fastq; do
  seqtk trimfq -q 0.02 -l 350 $i > ./trim/${i[@]/.fastq/_trim.fastq}
done

#!/bin/bash

#SBATCH --job-name=tcr_mixcr                   #This is the name of your job
#SBATCH --cpus-per-task=12                  #This is the number of cores reserved
#SBATCH --mem-per-cpu=1G              #This is the memory reserved per core.
#Total memory reserved: 12GB

#SBATCH --time=06:00:00        #This is the time that your task will run
#SBATCH --qos=6hours           #You will run in this queue

# Paths to STDOUT or STDERR files should be absolute or relative to current working directory
#SBATCH --output=tcr_mixcr     #This is the joined STDOUT and STDERR file
#SBATCH --mail-type=END,FAIL,TIME_LIMIT
#SBATCH --mail-user=julian.spagnuolo@unibas.ch        #You will be notified via email when your task ends or fails

assCon=$1
input=$2

module load MiXCR/3.0.3-Java-1.8.0_92

mixcr align -s hsa --threads 12 -p rna-seq -OallowPartialAlignments=true -OsaveOriginalReads=true -OvParameters.geneFeatureToAlign=VTranscriptWithout5UTRWithP --not-aligned-R1 ${input[@]/.fastq/_unaligned.fastq} --report ${input[@]/.fastq/_align_report.txt} $input ${input[@]/.fastq/.vdjca}

if [ "$assCon" = "true" ]; then
  # assemble clonotypes based on --region-of-interest
  mixcr assemble --threads 12 -OseparateByV=true -OseparateByJ=true -OseparateByC=false --report ${input[@]/.fastq/_ass_report.txt} --write-alignments ${input[@]/.fastq/.vdjca} ${input[@]/.fastq/.clna}
  
  # assemble contigs: execute only if --assembleContigs is specified
  mixcr assembleContigs ${input[@]/.fastq/.clna} ${input[@]/.fastq/.clns}
  
  # export to tsv
 mixcr exportClones -o -t -c TRB -cloneId -count -fraction -vHit -dHit -jHit -cHit -vGene -dGene -jGene -cGene -vFamily -dFamily -jFamily -cFamily -vHitScore -dHitScore -jHitScore -cHitScore -nFeature CDR3 -nFeature VCDR3Part -nFeature DCDR3Part -nFeature JCDR3Part -nFeature VJJunction -nFeature VDJunction -nFeature DJJunction -aaFeature CDR3 -aaFeature VCDR3Part -aaFeature DCDR3Part -aaFeature CDR1 -aaFeature CDR2 -chains ${input[@]/.fastq/.clns} ${input[@]/.fastq/_results.txt}
elif [ "$assCon" = "false" ]; then
  # assemble clonotypes based on --region-of-interest
  mixcr assemble --threads 12 -OseparateByV=true -OseparateByJ=true -OseparateByC=false --report ${input[@]/.fastq/_assc_report.txt} --write-alignments ${input[@]/.fastq/.vdjca} ${input[@]/.fastq/.clna}
  
  # export to tsv
  mixcr exportClones -o -t -c TRB -cloneId -count -fraction -vHit -dHit -jHit -cHit -vGene -dGene -jGene -cGene -vFamily -dFamily -jFamily -cFamily -vHitScore -dHitScore -jHitScore -cHitScore -nFeature CDR3 -nFeature VCDR3Part -nFeature DCDR3Part -nFeature JCDR3Part -nFeature VJJunction -nFeature VDJunction -nFeature DJJunction -aaFeature CDR3 -aaFeature VCDR3Part -aaFeature DCDR3Part -aaFeature CDR1 -aaFeature CDR2 -chains ${input[@]/.fastq/.clna} ${input[@]/.fastq/_results.txt}
fi

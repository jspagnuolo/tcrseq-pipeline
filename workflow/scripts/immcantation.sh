path=$1
threads=$2


cd $path
for f in all_contig.fasta filtered_contig.fasta filtered_contig_annotations.csv all_contig_annotations.csv ; do cp $f $f.cellranger ;done

# Generate AIRR Rearrangement data from the 10X V(D)J FASTA files
AssignGenes.py igblast -s ./filtered_contig.fasta -b /usr/local/share/igblast --organism human --loci tr --format blast --nproc $threads --outdir ./
MakeDb.py igblast -i ./filtered_contig_igblast.fmt7 -s ./filtered_contig.fasta -r /usr/local/share/germlines/imgt/human/vdj/imgt_human_*.fasta --10x $./filtered_contig_annotations.csv --extended --outdir ./

# Splitting into separate light and heavy chain files
# To group B cells into clones from AIRR Rearrangement data, the output from MakeDb must be parsed into a light chain file and a heavy chain file:

ParseDb.py select -d ./filtered_contig_igblast_db-pass.tsv -f locus -u "TRA" --logic all --regex --outname tcrA --outdir ./
ParseDb.py select -d ./filtered_contig_igblast_db-pass.tsv -f locus -u "TRB" --logic all --regex --outname tcrB --outdir ./


import glob
import os


### what does all this do???
# Create sym-links and target lists
files = glob.glob(os.path.join(indir, "*_R1.fastq.gz"))

finished = set()
samples = dict()
for f in files:
    bname = os.path.basename(f)[:-12]  # Trim the path and _R1.fastq.gz
    pname = bname[:-2]  # Trim the replicate letter (e.g., _A or _B)
    os.makedirs(os.path.join(outdir, "fastq", pname), exist_ok=True)
    if pname not in samples:
        samples[pname] = []
    samples[pname].append(bname)
    R1 = "{}_S1_L001_R1_001.fastq.gz".format(os.path.join(outdir, "fastq", pname, bname))
    R2 = "{}_S1_L001_R2_001.fastq.gz".format(os.path.join(outdir, "fastq", pname, bname))
    if not os.path.exists(R1):
        os.symlink(f, R1)
    if not os.path.exists(R2):
        os.symlink("{}_R2.fastq.gz".format(f[:-12]), R2)
    finished.add("{}_finished".format(os.path.join(outdir, pname)))

rule gex:
    input:
		{fastq_path} = "/path/to/NAS/{experiment}/data/fastq/"
    output:
        "/path/to/NAS/{experiment}/results/gex/{sample}/"
    container:
        "docker://cellranger" ##check his
    log: "/path/to/NAS/{experiment}/results/gex/{sample}/cell_count.log"
    threads: 16
    message: "Getting GEX using {threads} threads"
    shell:
		"""
		cellranger count \
		--transcriptome= "/refdata-cellranger-vdj-GRCh38-alts-ensembl-5.0.0" \
		--fastqs {fastq_path} \
		--id {sample} \ ## TODO: make text parser to generate samples from fq file names; ie. split sampleID_S1_R1_L1.fq at first _
		--sample {sample} \
		--localcores={threads} \
		--localmem=128 2> {log} \
		--no-bam \ ## don't spit out the bulky bam file
		--nosecondary \ ## no secondary analysis (speed up this step)
		--disable-ui
		"""